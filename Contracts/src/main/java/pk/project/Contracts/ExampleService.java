package pk.project.Contracts;

/**
 * Public API representing an example OSGi service
 */
public interface ExampleService
{
    // public methods go here...

    String scramble( String text );
}


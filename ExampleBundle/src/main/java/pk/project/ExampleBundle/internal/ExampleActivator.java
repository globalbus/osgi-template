package pk.project.ExampleBundle.internal;

import java.util.Dictionary;
import java.util.Properties;

import org.osgi.framework.BundleContext;


import pk.project.Contracts.ExampleService;
import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Deactivate;

/**
 * Extension of the default OSGi bundle activator
 */
@Component(immediate=true)
public final class ExampleActivator
{
    /**
     * Called whenever the OSGi framework starts our bundle
     */
    @Activate
    public void activate( BundleContext bc )
        throws Exception
    {
        System.out.println( "STARTING ExampleBundle" );

        Dictionary props = new Properties();
        // add specific service properties here...

        System.out.println( "REGISTER ExampleBundle.ExampleService" );

        // Register our example service implementation in the OSGi service registry
        bc.registerService( ExampleService.class.getName(), new ExampleServiceImpl(), props );
    }
    public ExampleActivator(){
        System.out.println( "Construct ExampleBundle" );
        
    }
    /**
     * Called whenever the OSGi framework stops our bundle
     */
    @Deactivate
    public void stop( BundleContext bc )
        throws Exception
    {
        System.out.println( "STOPPING ExampleBundle" );

        // no need to unregister our service - the OSGi framework handles it for us
    }
}

